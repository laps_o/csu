import smbus

#Check this! this is for RPi

try:
	bus=smbus.SMBus(1)
except Exception, e:
	print "importing smbus fail, is this a Raspy?"

#Maximum number of bytes for I2C - 1
MAXBLOCK = 31


class Command(object):
	""" Class for I2C Command for a single servo driver
		General Attributes:
			ADDR: I2C addres for the driver
			nservos: number of controlled servos
			size: Command block length in bytes excluding command byte (default MAXBLOCK)
			cmd: command byte (see doc in Arduino slave program)
		Attributes for building Block Type 1 (moving servos):
			target: target of movement in pulse width us
			speed: rate of pulse width increment during movement in clock ticks
			delay: delay for starting movement in number of cycles
		Attributes for building Block Type 2 (adjust starting phase):
			target: number of cycles for starting phase
			speed: rate of pulse width increment during starting phase in ticks
		Attributes for building Block Type 3 (calibrating servos):
			numservo: number of servo to be calibrated
			uminc:  rectification for pulsewidth corresponding to 0 deg in us
		        umaxc: rectification for pulsewidth corresponding to 180 deg in us
		Main Methods:
			write_reset(): reset all servos to UINI (defined by drivers = 90)
			write_block(): write command to servo driver (all Block types)
			read_Value(): make a request to servo drive and returns al servo 
				positions in pulse width us
		Todo:
			time: incorporate time for triggered action

	"""
	def __init__(self,ADDR,nservos,size=MAXBLOCK):
		"""
		Arguments:
			ADDR: servo driver I2C address
			nservos: number of servos controlled by driver
			size: maximum size of Command block
		"""
		self.ADDR = ADDR
		self.nservos = nservos
		self.size = size
		self.cmd = None
		self.target = None
		self.speed = None
		self.delay = None
		self.time = 0
		self.numservo = None
		self.uminc = 0
		self.umaxc = 0	
	def set_block(self,**kwargs):
		for key, value in kwargs.items():
			self.__setattr__(key,value)
	def write_reset(self):
		try: 
                        print "writing block", self.cmd
			bus.write_byte_data(self.ADDR,0,1)
			return 1
		except IOError:
			print "Error accediendo 0x%02X" % self.ADDR
			return -1
	def write_block(self):
		bloque = [0] * self.size 
		if self.cmd < 11:
			for n in range(self.nservos):
				bloque[3*n+1] = int(self.target[n]) >> 8
				bloque[3*n+2] = int(self.target[n]) & 0xff
			if self.cmd < 3:
				bloque[0]=int(self.delay[0]) & 0xff
				for n in range(self.nservos):
					bloque[3*n+3] = int(self.speed[n]) & 0xff
			elif self.cmd == 3:
				bloque[0]=int(self.speed[0]) & 0xff
				for n in range(self.nservos):
					bloque[3*n+3] = int(self.delay[n]) & 0xff
			else:
				pass
		elif self.cmd == 11:
			bloque[0] = self.numservo
			bloque[1] = self.uminc >> 8
			bloque[2] = self.uminc & 0xff
			bloque[3] = self.umaxc >> 8
			bloque[4] = self.umaxc & 0xff
		else:
			return -1
		try: 
			print "writing block", self.cmd, bloque
			bus.write_i2c_block_data(self.ADDR,self.cmd,bloque)
			return 1
		except IOError:
			print "Error accediendo 0x%02X" % self.ADDR
			return -1
	
	def readValue(self):
		pos = [0] * self.nservos
		try:
			bloque = bus.read_i2c_block_data(self.ADDR,0)
			#act = bloque[1] + (bloque[0] << 8)
			for n in range(self.nservos):
				pos[n] = bloque[2*n+1] + (bloque[2*n] << 8)
			return pos	
		except IOError:
			print "Error recibiendo 0x%02X" % self.ADDR
			return None 


class Gesture(object):
	""" Class for Gesture (list of Commands for all servos)
	Attributes:
		cmd: List of nctrl Commands for each servo driver
		nctrl: number of servo drivers
		nservos: number of servos controlled by each servo driver
		times: list of times for sending Commands (not yet implemented)
	Main Methods:
		set_blocks(**kwargs): set attributes for Commands used to build blocks
			Note that cmd must be a 1D list and all other attributes 2D lists
		reset(): reset all servos invoking write_reset() for each servo driver
		commit(): send all the Commands to the servo drivers
	Todo:
		Scheduler for commit
	"""
	def __init__(self,ADDR,nservos,times=None):
		"""
		Arguments:
			ADDR: List of addresses for all servo drivers
			nservos: number of servos controlled by each servo driver
		"""
		self.nservos = nservos
		self.times = times
		self.nctrl = len(ADDR)
		self.cmd = [Command(ADDR[i],nservos) for i in range(self.nctrl)]
	def __iter__(self):
	        return iter(self.cmd)
	def __getitem__(self,index):
	        return self.cmd[index]
	def set_blocks(self,**kwargs):
		for n,ctrl in enumerate(self):
			for key, value_list in kwargs.items():
				value=value_list[n]
				print  "key:values", key, value
				ctrl.__setattr__(key,value)
	def reset(self):
		for ctrl in self:
			ctrl.write_reset()
	def commit(self):
		for ctrl in self:
			ctrl.write_block()

class Calibration(Gesture):
	""" Class for Calibration Commands for all servos
		PENDIENTE
	"""
	def __init__(self,ADDR,nservos,servo_cal):
		Gesture.__init__(self,ADDR,nservos)
		self.servo_cal = servo_cal

