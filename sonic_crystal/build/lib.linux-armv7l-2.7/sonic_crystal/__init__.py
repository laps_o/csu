from commands import Gesture


atous = [548, 558, 569, 579, 589, 600, 610, 620, 631, 641, 651, 662, 672, 682, 692, 703, 713, 723, 734, 744, 754, 765, 775, 785, 796, 806, 816, 827, 837, 847, 857, 868, 878, 888, 899, 909, 919, 930, 940, 950, 961, 971, 981, 992, 1002,  1012,  1022,  1033,  1043,  1053,  1064,  1074,  1084, 1095,  1105,  1115,  1126,  1136,  1146,  1156,  1167,  1177,  1187,  1198,  1208,  1218,  1229,  1239,  1249,  1260,  1270,  1280,  1291,  1301,  1311,  1321,  1332,  1342,  1352,  1363,  1373,  1383,  1394,  1404,  1414,  1425,  1435,  1445,  1456,  1466,  1476,  1486,  1497,  1507,  1517,  1528,  1538,  1548,  1559,  1569,  1579,  1590,  1600,  1610,  1620,  1631,  1641,  1651,  1662,  1672,  1682,  1693,  1703,  1713,  1724,  1734,  1744,  1755,  1765,  1775,  1785,  1796,  1806,  1816,  1827,  1837,  1847,  1858,  1868,  1878,  1889,  1899,  1909,  1920,  1930,  1940,  1950,  1961,  1971,  1981,  1992,  2002,  2012,  2023,  2033,  2043,  2054,  2064,  2074,  2084,  2095,  2105,  2115,  2126,  2136,  2146,  2157,  2167,  2177,  2188,  2198,  2208,  2219, 2229,  2239,  2249,  2260,  2270,  2280,  2291,  2301,  2311,  2322,  2332,  2342,  2353,  2363,  2373,  2383,  2394,  2400]



class Servo(object):
	""" Class for servo object
	Attributes:
		row: row index for Servo
		col: column index for servo
		ctrl: index for servo driver controlling Servo
		nums: index for Servo inside servo driver
		pos: Servo position (in pulsewidth us)
		uminc: rectification for pulsewidth corresponding to 0 deg in us
		umaxc: rectification for pulsewidth corresponding to 180 deg in us
		ntrans: number of cycles for the starting phase
		speed2: increment for the starting phase in clock ticks
		[Unused, now incoroporated to Command: target, speed, delay]
	"""
	def __init__(self, fila, columna, nfilas, cols_ctrl):
		"""
		Args: 
			fila: row for Servo
			columna: column for Servo
			nfilas: total number of columns
			cols_ctrl: number of columns controlled by each servo driver	
		"""
		self.row = fila
		self.col = columna
		self.ctrl = columna / cols_ctrl
		self.nums = nfilas*(columna % cols_ctrl) + fila
		self.pos = None
		self.ntrans = None
		self.speed2 = None
		self.uminc = 0
		self.umaxc = 0
		self.target = None
		self.delay = 0
		self.speed = None
	def __call__(self, target,speed,delay):
		self.target, self.speed, self.delay = target, speed, delay
			
class Modulo(object):
	""" Main Class for Sonic Crystal Module
	Attributes:
		s: 2D list of Servos of size ncols x nfilas 
		gestures: list of Gestures (one for immediate execution and npedals for triggered action)
		ADDR: list of I2C addresses for the servo drivers
		nctrl: number of servo drivers
		nservos: number of servos for driver
	Main methods:
		set_srv_ctrl(**kwargs): set attributes for all Servos using 2D lists arranged in  
			nctrl x nservos as arguments 
		set_srv_col(**kwargs): set attributes for all Servos using 2D lists arranged in ncols x nfilas as
			arguments
		set_gestures_col(**kwargs): set attributes and blocks for nctrl commands in a specific gesture. This 
			function is used to 'load' a Gesture in the class
		set_pos(): uses gestures[0] to read all positions from the servo drivers (using method 
			read.Value from class Command), set all Servos attribute pos to these values 
			and also returns the positions as a 2D list.
		make_gestures(): executes a loaded Gesture	
		.. get* .. for debugging
	Todo:
		add a Method to detect file changes and load attributes and blocks to gestures and s
	"""
	def __init__(self, nfilas, ncols, cols_ctrl,npedals=3,ADDR=None):
		"""
		Arguments:
			nfilas: number of rows
			ncols: number of columns
			cols_ctrl: number of servos controlled by each servo driver
			npedals: number of tiggered actions (pedals) Default 3
			ADDR: I2C addresses of servo drivers. Default 3 ... 3 + number o drivers -1
		"""
		self.s =  [[Servo(i,j,nfilas,cols_ctrl) for i in range(nfilas)] for j in range(ncols)]
		self.cols_ctrl = cols_ctrl
		self.nctrl = ncols / cols_ctrl
		self.nservos = cols_ctrl * nfilas
		if ADDR == None:
			self.ADDR = range(3,3+self.nctrl)	
		else:
			self.ADDR = ADDR
		self.gestures = [Gesture(self.ADDR,self.nservos) for i in range(npedals+1)]
		self.mtime = None
		self.busy = None
	def __iter__(self):
		return iter(self.s)
	def __getitem__(self,index):
		return self.s[index] 
	def set_srv_ctrl(self,**kwargs):
		for col in self:
			for srv in col:
				for key, value_list in kwargs.items():
					value = value_list[srv.ctrl][srv.nums]
					srv.__setattr__(key,value)
	def set_srv_col(self,**kwargs):
		for col in self:
			for srv in col:
				for key, value_list in kwargs.items():
					value = value_list[srv.col][srv.row]
					srv.__setattr__(key,value)
	def set_gestures_ctrl(self,n_gesture,**kwargs):
		self.gestures[n_gesture].set_blocks(**kwargs)
		
	def get_pos(self):
		pos_ctrl = []
		for ctrl in self.gestures[0]:
			temp = ctrl.readValue()
			(pos_ctrl.append(temp) if temp is not None else None)
		if len(pos_ctrl) == self.nctrl: 	
			self.set_srv_ctrl(pos = pos_ctrl)	
			return pos_ctrl
	def get_ctrl(self,attr):
		ret = []
		rcol = []
		for ncol, col in enumerate(self):
			for srv in col:
				rcol.append(srv.__getattribute__(attr))
			if (ncol+1) % self.cols_ctrl == 0:
				ret.append(rcol)
				rcol = []
		return ret	
	def get_col(self,attr):
		ret = []
		for col in self:
			rcol =[]
			for srv in col:
				rcol.append(srv.__getattribute__(attr))
			ret.append(rcol)
		return ret
	def get_gestures(self,attr):
		ret = []
		for n_gesture in self.gestures:
			rgestures = []
			for ctrl in n_gesture:
				rgestures.append(ctrl.__getattribute(attr))
			ret.append(rgestures)
		return ret	
	def make_gesture(self,n_gesture,times=0):
		self.gestures[n_gesture].commit()
	def reset(self,n_gesture):
		self.gestures[n_gesture].reset()
