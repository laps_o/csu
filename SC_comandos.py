import sonic_crystal as sc
from sonic_crystal import atous
import RPi.GPIO as GPIO

GPIO.setmode(GPIO.BCM)
GPIO.setup(17, GPIO.IN, pull_up_down=GPIO.PUD_UP)
GPIO.setup(27, GPIO.IN, pull_up_down=GPIO.PUD_UP)
GPIO.setup(22, GPIO.IN, pull_up_down=GPIO.PUD_UP)

#Globar vb only for creating the sc module and the refresh rate
nfilas = 5
cols_ctrl = 2
n_ctrl = 6 
NSERVOS = cols_ctrl * nfilas
ncols = cols_ctrl * n_ctrl
lapso = 1

ADDR=[0x08]
#ADDR=[0x04,0x05,0x06,0x07,0x08,0x09]
stat = [0]

cs = sc.Modulo(nfilas,ncols,cols_ctrl,3,ADDR)
# cs.gestures[0].reset()

pos_ctrl = cs.get_pos()
print "pos", pos_ctrl

def goto(ang,speed):
	cmd = []
	tar = []
	spd = []
	dly = []
	for n in range(n_ctrl):
		tar.append([atous[ang] for i in range(NSERVOS)])
		cmd.append(1)
		spd.append([speed]*NSERVOS)
		dly.append([0]*NSERVOS)
	cs.set_gestures_ctrl(0,cmd=cmd,target=tar,speed=spd,delay=dly)
	cs.make_gesture(0)

def gotodif(ang,speed,ncdif,nsdif,angdif):
	cmd = []
	tar = []
	spd = []
	dly = []
	for n in range(n_ctrl):
		tar.append([atous[ang] for i in range(NSERVOS)])
		cmd.append(1)
		spd.append([speed]*NSERVOS)
		dly.append([0]*NSERVOS)
        tar[ncdif][nsdif] = atous[angdif]        
	cs.set_gestures_ctrl(0,cmd=cmd,target=tar,speed=spd,delay=dly)
	cs.make_gesture(0)

def read_stat(filename,line0=0):
	"""Funcion provisoria para leer estado de un archivo de texto"""
	f = open(filename,'r')
	lines = [line.rstrip('\n') for line in f]
	cmda = int(lines[0])
	spda = int(lines[1])
	dlya = int(lines[2])
	cmd = []
	tar = []
	spd = []
	dly = []
	for line in lines[3:]:
		tar.append([atous[int(s)] for s in line.split()])
		cmd.append(cmda)
		spd.append([spda]*NSERVOS)
		dly.append([dlya]*NSERVOS)
	f.close()	
	return cmd,tar,spd,dly

def read_set__gesture(gest_file,n_gest=0):
	cmd, tar, spd, dly = read_stat(gest_file)
	cs.set_gestures_ctrl(n_gest,cmd=cmd,target=tar,speed=spd,delay=dly)

def read_make__gesture(gest_file,n_gest=0):
	cmd, tar, spd, dly = read_stat(gest_file)
	cs.set_gestures_ctrl(n_gest,cmd=cmd,target=tar,speed=spd,delay=dly)
	cs.make_gesture(n_gest)

def reset(n_gest=0):
        cs.reset(n_gest)
