// VarSpeed ServoArray I2C Slave version 1.0
// Recibe comandos por I2C de hasta 32 bytes
// Para mover hasta 10 servos.
// Con la sintaxis:
// CMD PAR (TH0 TL0 P0 TH1 TL1 P1 ... TH9 TL9 P9)  
// CMD 0 Reset all servos to USINI position
// CMD 1 Move servos synchronously at variable speed
// CMD 2 Move servos with fixed delay by columns at variable speed
// CMD 3 Move servos with variable delay at fixed speed
// CMD 4 Move servos synchronously to angle (maximum speed)
// CMD 5 Move servos with fixed delay by columns to an angle (not yet implemented)
// CMD 6 Move servos with variable delay to a fixed angle (not yet implemented)
// CMD 7-9 Unused
// CMD 10 Set starting phase
// CMD 11 Set offsets for servos
// CMD 12 Set control parameters 
// THN TLN son el byte high y low del target en us para el servo N
// PN es el incremento en ticks para los CMD 1-3, el delay para CMD 4-8 (y nada para CMD 5-7)
// PAR es el delay entre columnas para los comandos 3 y 7 y el incremento en ticks para CMD 4
// 

#include <VarSpeedServo.h>
#include <WSWire.h>
#define NSERVOS 10 
#define NCOL 5
#define PIN_0 2         //Pin inicial de la conexion Nano 2 Promini 4
#define MAX_LENGTH 32   //Tamano del bloque de I2C
#define USMIN 544       
#define USMAX 2400
#define USINI 1476
#define SL_ADDR 0x08    // Address del Slave varia para cada Arduino en el rango 0x04 0x09

 
VarSpeedServo servos[NSERVOS];
  
boolean commandComplete = false;
byte command[MAX_LENGTH];
byte command_length;
int act_servos = 0;                //actividad de servos en bits, modificado por loop y processCommand
const int mask_0 = 1 << NSERVOS-1;   // mascara de bits inicial para act_servos 

int  updateInterval = 5;    
unsigned long lastUpdate;

void setup()
{
  Wire.begin(SL_ADDR);                
  Wire.onReceive(receiveEvent); // Registra evento de recibir comando
  Wire.onRequest(requestEvent); // Registra evento de request de posicion
  Serial.begin(230400);         //Eliminar serial!
  command_length = 0;
  for (byte n = 0; n<NSERVOS; n++){
    pinMode(PIN_0+n, OUTPUT);
    Serial.print(n);
    Serial.println(" > Inicio");
    servos[n].attach(PIN_0+n,USMIN,USMAX);
    servos[n].n_trans=60;
    servos[n].sp2=2;
    servos[n].del=0;
    servos[n].usminc=0;
    servos[n].usmaxc=0;
    servos[n].writeMicroseconds(USINI);
  }  
  delay(1000);
  for (byte n = 0; n<NSERVOS; n++){  
    servos[n].pos_us=servos[n].readMicroseconds();
    servos[n].detach();
    digitalWrite(PIN_0+n, LOW);
  }
}


void loop() {
  if (commandComplete == true) {
    for (byte n = 0; n < command_length; n++) {
      Serial.print(command[n]);
      Serial.print(" ");
    } 
    lastUpdate = millis();
    processCommand();
  }
  if (act_servos>0) {
    //al menos algun servo esta activo, actualiza su posicion
    int mask = mask_0;
    if((millis() - lastUpdate) > updateInterval) {
      lastUpdate = millis();
      for (byte n = 0; n<NSERVOS; n++){ 
        if (mask & act_servos){
            //servo activo   
            servos[n].pos_us = servos[n].readMicroseconds();
            if (servos[n].pos_us == servos[n].tar_us) { 
              servos[n].detach(); // detachea 
             act_servos ^= mask; //inactiva el bit
                 //esto esta aca por el bug original de la libreria Servo con detach
                  digitalWrite(PIN_0+n, LOW);
            }
        }
        mask >>= 1;
      }  
    }
  }  
}

//Mantener al minimo, solo levantando el comando y cambiando el flag 
//de que hay un comando en el buffer. Recibe el comando
void receiveEvent(int numBytes)
{
  for (byte n = 0; n < numBytes; n++) {
    if (n < MAX_LENGTH)
    {
      command[n] = Wire.read();
      command_length++;
    }  
    else {
      Wire.read();
    }
  }  
  commandComplete = true;
}

//Devuelve las posiciones de los servos en ancho de pulso
void requestEvent(){
   byte posiciones[NSERVOS*2];
   for (byte n = 0; n<NSERVOS; n++){
      posiciones[2*n] = highByte(servos[n].pos_us);
      posiciones[2*n+1] = lowByte(servos[n].pos_us); 
   }
   Wire.write(posiciones,NSERVOS*2);
}

//Procesa el comando
void processCommand(){
  //comandos mas frecuentes primero
  byte CMD = command[0];
  if ((CMD > 0 && CMD < 10) && command_length == MAX_LENGTH) {
    for (byte n = 0; n<NSERVOS; n++){
      if (servos[n].usminc) {
        servos[n].tar_us = map(word(command[3*n+2],command[3*n+3]),USMIN,USMAX,servos[n].usminc,servos[n].usmaxc); 
      }
      else {
        servos[n].tar_us = word(command[3*n+2],command[3*n+3]);
      }  
    }  
    if (CMD < 4) {
      //Variable Speed CMD PAR TH TL X TH TL X TH TL X .. 
      for (byte n = 0; n<NSERVOS; n++){
        servos[n].trans_us = transit(servos[n].tar_us,servos[n].pos_us,servos[n].n_trans);
      }  
    }
    if (CMD == 1) {
       //Variable Speed PAR nada X sp1
      int mask = mask_0;
      for (byte n = 0; n<NSERVOS; n++){
        servos[n].sp1 = command[3*n+4];
        if ((servos[n].sp1 > 1) && (servos[n].tar_us != servos[n].pos_us)) {
          if (!(servos[n].attached())) {
            servos[n].attach(PIN_0+n,USMIN,USMAX);
            act_servos ^= mask;
          }
          servos[n].push(servos[n].tar_us,servos[n].sp1,servos[n].trans_us,servos[n].sp2);
        }
        mask >>= 1;  
      }
    }
    if (CMD == 2) {
      //Variable Speed PAR delay X sp1
      int mask = mask_0;
      for (byte n = 0; n<NSERVOS; n++){
        servos[n].sp1 = command[3*n+4];
        if ((servos[n].sp1 > 1) && (servos[n].tar_us != servos[n].pos_us)) {
          if (!(servos[n].attached())) {
            servos[n].attach(PIN_0+n,USMIN,USMAX);
            act_servos ^= mask;
          }
          if ( n<NCOL ) {
            servos[n].push(servos[n].tar_us,servos[n].sp1,servos[n].trans_us,servos[n].sp2);
          }
          else {
            servos[n].push(servos[n].tar_us,servos[n].sp1,servos[n].trans_us,servos[n].sp2,command[1]);  
          }
        } 
        mask >>= 1;   
      }
    }
    if (CMD == 3) {
       //Fixed Speed PAR sp1 X delay
      int mask = mask_0;
      for (byte n = 0; n<NSERVOS; n++){
        servos[n].del = command[3*n+4];
        servos[n].sp1 = command[1];
        if(servos[n].tar_us != servos[n].pos_us) {
          if (!(servos[n].attached())) {
            servos[n].attach(PIN_0+n,USMIN,USMAX);
            act_servos ^= mask;
          }
          servos[n].push(servos[n].tar_us,servos[n].sp1,servos[n].trans_us,servos[n].sp2,servos[n].del);  
        }  
        mask >>= 1;  
      }
    }
    if (CMD == 4) {
      //Move to a fixed angle CMD PAR TH TL X TH TL X TH TL X .. X 0 o 1
      int mask = mask_0;
      for (byte n = 0; n<NSERVOS; n++){
        servos[n].sp1 = command[3*n+4]; //0 o 1 mueve o no mueve
        if ((servos[n].sp1 > 1) && (servos[n].tar_us != servos[n].pos_us)) {
          if (!(servos[n].attached())) {
            servos[n].attach(PIN_0+n,USMIN,USMAX);
            act_servos ^= mask;
          }
          servos[n].writeMicroseconds(servos[n].tar_us);
        }
        mask >>= 1;  
        delay(100);
      }
      
    }
  }
  else if (CMD == 10 && command_length == MAX_LENGTH) {
    //Set starting phase for each servo CMD PAR NTH NTL SP2 NTH NTL SP2 NTH NTL SP2....
     for (byte n = 0; n<NSERVOS; n++){
       servos[n].n_trans = word(command[3*n+2],command[3*n+3]);
       servos[n].sp2 =  command[3*n+4];
     }
  }
  else if (CMD == 11 && command_length == 7) {
    //Set offset for one servo CMD PAR NSERVO USMINH USMINL USMAXH USMAXL
    byte n = command[2];
    if (n < NSERVOS) {
      servos[n].usminc = word(command[3],command[4]);
      servos[n].usmaxc = word(command[5],command[6]);  
    }
  }
  else if (CMD == 0 && command_length == 2) {
    //RESET
    int mask = mask_0;
    for (byte n = 0; n<NSERVOS; n++){
      if (!(servos[n].attached())) {
        servos[n].attach(PIN_0+n,USMIN,USMAX);
        act_servos ^= mask;
      }
      servos[n].tar_us = map(USINI,USMIN,USMAX,USMIN+servos[n].usminc,USMAX+servos[n].usmaxc);
      servos[n].writeMicroseconds(servos[n].tar_us);
      mask >>= 1;   
      delay(100);
    }
    
  }
  command_length = 0;
  commandComplete = false;
}

int transit(int tar, int pos, byte n) {
  int trans;
  if (n < abs(tar - pos)) {
    if (tar > pos) {
      trans = pos + n;
    }
    else {
      trans = pos - n;
    }
  }
  else {
    trans = tar;
  }
  return trans;
}

